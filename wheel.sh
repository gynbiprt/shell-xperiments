tput civis -- invisible
red='\e[0;31m'
NC='\e[0m' # No Color
for r in {1..3}
  do
    for i in {\|,/,-,\\}
      do 
      tput sc
      echo -ne "(${red}$i"
      echo -ne "${NC})"
      sleep 1
      tput el1
      tput rc
    done
done
tput cnorm -- normal
