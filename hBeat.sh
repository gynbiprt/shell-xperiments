clear
trap "tput sgr0; clear;tput cnorm -- normal; exit" SIGTERM SIGINT
r=`tput lines`
c=`tput cols`
tput cup $((${r}/2)) $((${c}/2))
tput civis -- invisible
redFr='\e[0;31m'
NC='\e[0m' # No Color
h1="\u2764"
h2="\u2665"
beatTime=0.8571

for r in {1..3}
  do
     tput sc
     echo -ne "${redFr}${h1}"
     #printf %b "${h1}"
     echo -ne " ${NC} "
     sleep ${beatTime}
     tput el1
     tput rc
     
     tput sc
     echo -ne "${redFr}${h2}"
     #printf %b "${h2}"
     echo -ne " ${NC} "
     sleep ${beatTime}
     tput el1
     tput rc

done
tput cnorm -- normal
tput cup 0 0

### below is one liner for above
### clear;trap "tput sgr0; clear;tput cnorm -- normal; exit" SIGTERM SIGINT;r=`tput lines`;c=`tput cols`;tput cup $((${r}/2)) $((${c}/2));tput civis -- invisible;redFr='\e[0;31m';NC='\e[0m';h1="\u2764";h2="\u2665";beatTime=0.8571;for r in {1..30};do tput sc;echo -ne "${redFr}${h1}";echo -ne " ${NC} ";sleep ${beatTime};tput el1;tput rc;tput sc;echo -ne "${redFr}${h2}";echo -ne " ${NC} ";sleep ${beatTime};tput el1;tput rc;done;tput cnorm -- normal;tput cup 0 0;
